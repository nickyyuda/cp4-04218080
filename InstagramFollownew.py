from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from datetime import datetime
import time

driver = webdriver.Chrome()
url = "https://www.instagram.com/accounts/login/?source=auth_switcher"
driver.get(url)

email = input('Enter Username Login : ')
password = input('Enter Password Login : ')
email_box = driver.find_element_by_name('username')
email_box.send_keys(email)
password_box = driver.find_element_by_name('password')
password_box.send_keys(password)
login = driver.find_element_by_class_name('L3NKy')
login.click()
username = input('Enter Username Target : ')
count = int(input('Input Looping : '))
seconds = int(input('Jeda Setiap Follow : '))
url = ('https://www.instagram.com/{}'.format(username))
print("Waiting To Loading Website...")
driver.get(url)
time.sleep(5)
followers = driver.find_element_by_css_selector('a.-nal3')
followers.click()
time.sleep(5)
print("Starting to Follow...")

for i in range(1, count+1):
        try:
                follow = driver.find_element_by_xpath('//button[text()="Follow"]')
                follow.click()
                print(i,".", datetime.now(), "| Success Follow")
                print("Waiting {} Seconds To Follow Again...".format(seconds))
                time.sleep(seconds)
        except NoSuchElementException:
                print(i,".", datetime.now(), "| Failed to Follow")
                print("Waiting {} Seconds To Follow Again...".format(seconds))
                time.sleep(seconds)
                raise Exception(NoSuchElementException)
